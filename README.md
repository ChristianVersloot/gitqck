# gitqck
`gitqck` is a three-line shell script which makes a developer's life easier. Commit-push actions towards a current Git branch usually take quite some typing work in a terminal: the commit, the reason, the push command and possibly the `git add` command.

`gitqck` removes this altogether. The shell script asks for the reason as input and subsequently fires the entire chain of commands.

How to use this script?

1. Clone the repository.
2. Move the `gitqck.sh` file to the repository in which you'd like to use it.
3. Add `gitqck.sh` to your `.gitignore` file.
4. When you want to commit and push, execute `./gitqck.sh` in a terminal, using the folder of the repository as your cwd.